I didn't get to a lot of my project goals.
I wanted procedurally generated terrain, towns, and events.
The actual prototype with static elements took a lot longer
than I thought, so I didn't get to the random stuff. Also,
it requires external jsons or something like that, which is
another thing to do. I needed to make sure I actually did
all the requirements.

I didn't have many system improvements, but I did make some
generic methods to cut down on code size. I chose to navigate
using dialogue loops, which worked out well but isn't the 
intended gameplay, but it was easier to implement.

I want to add the above-stated features, and I've written my
plans on my itch.io page, which I will update as I continue
to fix my game.