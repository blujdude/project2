﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public static GameManager instance;

	public int maxFuel = 40;
	public int maxMoney = 2000;
	public int maxBluffs = 5;
	public int currentFuel, currentMoney, currentBluffs;

	public Bar fuelBar, moneyBar, bluffBar;

	public Camera camera;
	public Text cityName;
	public Image cityNameBox;
	public Image buttonBox;
	public Image enterTownBox;
	public GameObject city;
	private Button[] buttons;
	private Button[] townButtons;
	private Transform[] cities = new Transform[17];
	Dictionary<string, string[]> connections = new Dictionary<string, string[]>();
	private Transform currCity;
	public GameObject train;


	// Start is called before the first frame update
	void Start()
	{
		instance = this;
		currentFuel = maxFuel;
		currentMoney = maxMoney;
		currentBluffs = maxBluffs;
		fuelBar.SetMaxValue(maxFuel);
		moneyBar.SetMaxValue(maxMoney);
		bluffBar.SetMaxValue(maxBluffs);
		cityName.enabled = false;
		cityNameBox.enabled = false;
		buttons = buttonBox.GetComponentsInChildren<Button>();
		townButtons = enterTownBox.GetComponentsInChildren<Button>();
		ToggleSelectionBox(false);
		ToggleEnterTown(false);
		Transform[] temp = city.GetComponentsInChildren<Transform>();
		int j = 0;
		foreach(Transform i in temp) {
			if(i.tag == "city") {
				cities[j++] = i;
			}
		}
		currCity = cities[0];
		SetConnections();
	}

    // Update is called once per frame
    void Update()
    {
		if (Input.GetMouseButtonDown(0)) {
			RaycastHit hit;
			Ray ray = camera.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit)) {
				GameObject parent = hit.transform.parent.gameObject;
				if (parent.tag == "city") {
					cityName.text = parent.name;
					cityName.enabled = true;
					cityNameBox.enabled = true;
				}
				else {
					cityName.text = "";
					cityNameBox.enabled = false;
				}
			}
			else {
				cityName.text = "";
				cityNameBox.enabled = false;
			}

		}
		if (Input.GetKey("escape")) {
			Application.Quit();
		}
	}

	void MinusFuel(int value)
	{
		currentFuel -= value;
		fuelBar.SetValue(currentFuel);
		
	}

	void AddFuel(int value)
	{
		currentFuel += value;
		fuelBar.SetValue(currentFuel);
	}

	void MinusMoney(int value)
	{
		currentMoney -= value;
		moneyBar.SetValue(currentMoney);
	}

	void AddMoney(int value)
	{
		currentMoney += value;
		moneyBar.SetValue(currentMoney);
	}

	void MinusBluffs(int value)
	{
		currentBluffs -= value;
		bluffBar.SetValue(currentBluffs);
	}

	void AddBluffs(int value)
	{
		currentBluffs += value;
		bluffBar.SetValue(currentBluffs);
	}

	public void SetConnections()
	{
		addConnection("Augusta", new string[] { "New York City", "", ""});
		addConnection("New York City", new string[] { "Pittsburgh", "", ""});
		addConnection("Pittsburgh", new string[] { "Chicago", "St. Louis", "Atlanta"});
		addConnection("Chicago", new string[] { "Pierre", "St. Louis", ""});
		addConnection("St. Louis", new string[] { "Chicago", "Denver", "New Orleans"});
		addConnection("Atlanta", new string[] { "St. Louis", "New Orleans", "Miami"});
		addConnection("Pierre", new string[] { "Helena", "Salt Lake City", "Denver"});
		addConnection("Denver", new string[] { "Salt Lake City", "Phoenix", "" });
		addConnection("Miami", new string[] { "New Orleans", "", "" });
		addConnection("New Orleans", new string[] { "Dallas", "", "" });
		addConnection("Dallas", new string[] { "Phoenix", "", "" });
		addConnection("Phoenix", new string[] { "Los Angeles", "", "" });
		addConnection("Salt Lake City", new string[] { "Los Angeles", "Gravity Falls", "" });
		addConnection("Gravity Falls", new string[] { "Seattle", "", "" });
		addConnection("Helena", new string[] { "Gravity Falls", "Seattle", "" });
		foreach (KeyValuePair<string, string[]> key in connections) {
			print(key);
		}
	}

	public void addConnection(string s1, string[] s2)
	{
		connections.Add(s1, s2);
	}

	public void DisplayDestinations()
	{
		int length = connections[currCity.name].Length;
		for (int i = 0; i < length; i++) {
			buttons[i].GetComponentInChildren<Text>().text = connections[currCity.name][i];
		}
		ToggleSelectionBox(true);
	}

	public void ToggleSelectionBox(bool toggle)
	{
		buttonBox.enabled = toggle;
		buttonBox.GetComponentInChildren<Text>().enabled = toggle;
		foreach (Button button in buttons) {
			if (button.GetComponentInChildren<Text>().text != "") {
				button.enabled = toggle;
				button.GetComponentInChildren<Text>().enabled = toggle;
			}
		}
	}

	public void SelectionA()
	{
		Selection(0);
	}

	public void SelectionB()
	{
		Selection(1);
	}

	public void SelectionC()
	{
		Selection(2);
	}

	public void Selection(int i)
	{
		foreach (Transform t in cities) {
			if (t.name == buttons[i].GetComponentInChildren<Text>().text) {
				currCity = t;
				break;
			}
		}
		GoToNextCity();
	}

	public void GoToNextCity()
	{
		ToggleSelectionBox(false);
		train.transform.LookAt(currCity);
		train.transform.rotation *= Quaternion.Euler(0, -90, 0);
		while(Vector3.Distance(currCity.position, train.transform.position) > 5){
			train.transform.position = Vector3.MoveTowards(train.transform.position, currCity.position, .0001f);
		}
		MinusFuel(4);
		if(currCity.name != "New York City" && currCity.name != "Pittsburgh") {
			MinusMoney(50);
		}
		if (currCity.name == "Miami") {
			AddFuel(12);
		}
		if (currCity.name == "Los Angeles" || currCity.name == "Seattle") {
			enterTownBox.enabled = true;
			enterTownBox.GetComponentInChildren<Text>().enabled = true;
			enterTownBox.GetComponentInChildren<Text>().text = "You Win!";
		}
		else {
			ToggleEnterTown(true);
		}
	}

	public void ToggleEnterTown(bool toggle)
	{
		enterTownBox.enabled = toggle;
		enterTownBox.GetComponentInChildren<Text>().enabled = toggle;
		foreach (Button button in townButtons) {
			button.enabled = toggle;
			button.GetComponentInChildren<Text>().enabled = toggle;
		}
	}

	public void Yes()
	{
		MinusBluffs(1);
		ToggleEnterTown(false);
		currCity.GetComponent<DialogueTrigger>().TriggerDialogue();
	}
	
	public void No()
	{
		ToggleEnterTown(false);
		DisplayDestinations();
	}
}
